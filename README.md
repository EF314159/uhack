µHack
====================

After several entire hours of coding, building, and procrastinating, I'm proud to present the best variant of The Best Game You Will Ever Play! µHack has a number of very cool and unique features, including:

* A MUCH shorter trip through Gehennom!
* A radically different endgame experience!
* A radically different early game experience!
* A 15-conduct ascension is possible!
* The Amulet of Yendor!
* Bugs! (probably)
